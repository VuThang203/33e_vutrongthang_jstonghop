// Kiểm tra số nguyên tố

function kiemTraSoNguyenTo(number) {
  var kiemTra = true;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      kiemTra = false;
      break;
    }
  }
  return kiemTra;
}

// Tìm số dương nhỏ nhất
function timSoMin(arr) {
  var min = arr[0];
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      min = arr[i];
      break;
    } else {
      min = `Không có số dương nào`;
    }
  }
  return min;
}

// Đảo ngược chuỗi
function daoNguocChuoi(number) {
  var ketQua = "";
  for (var i = number.length - 1; i >= 0; i--) {
    ketQua += number[i];
  }
  return ketQua;
}
// Random phần tử trong mảng
function ranDom(list) {
  list.sort(function () {
    return Math.random() - 0.5;
  });
  return list;
}
