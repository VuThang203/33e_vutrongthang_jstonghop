// Cau 1:

// Cách 1:
// var ketQua = "";
// var ketQua1 = "";
// for (soHang = 1; soHang <= 10; soHang++) {
//   for (soCot = soHang; soCot <= 100; soCot += 10) {
//     ketQua += soCot + " ";
//   }
//   ketQua += `<br/>`;
// }
// document.getElementById("result").innerHTML = ketQua;

// Cách 2:
var ketQua = "";
for (var b = 1; b <= 100; b++) {
  ketQua += b + " || ";
  if (b % 10 == 0) {
    ketQua += `<br/>`;
  }
  document.getElementById("result").innerHTML = ketQua;
}

// Cau 2:
var arrNum = [];
function themSoNguyenTo() {
  var number = document.querySelector("#txt-so-nguyen").value * 1;
  arrNum.push(number);
  document.getElementById("xuatmang").innerHTML = /*html*/ `
  <p>Array: ${arrNum}</p>
  `;
}
function inSoNguyenTo() {
  var ketQua = "";
  for (var index = 0; index < arrNum.length; index++) {
    var kiemTra = kiemTraSoNguyenTo(arrNum[index]);
    if (kiemTra && arrNum[index] > 1) {
      ketQua += arrNum[index] + ", ";
    }
  }
  document.querySelector("#result-1").innerHTML =
    `Các số nguyên tố trong mảng là: ` + ketQua;
}

// Cau 3:

function tinhTong() {
  var number = document.querySelector("#txt-nhap-n").value * 1;
  var ketQua = 0;
  if (number > 2) {
    for (var i = 2; i <= number; i++) {
      ketQua += i;
    }
    ketQua += 2 * number;
  } else {
    ketQua = `Vui lòng nhập số nguyên lớn hơn 2`;
  }
  document.querySelector("#result-2").innerHTML = ketQua;
}

// Cau 4:

function luongUocSo() {
  var number = document.querySelector("#txt-so-n").value * 1;
  var ketQua = "";
  for (var i = 1; i <= number; i++) {
    if (number % i == 0) {
      ketQua += i + " ";
    }
  }
  document.querySelector("#result-3").innerHTML =
    `Ước số của ` + number + ` là ` + ketQua;
}

// Cau 5:

function timSoDaoNguoc() {
  var number = document.querySelector("#txt-so-duong").value;
  var ketQua = daoNguocChuoi(number);
  document.querySelector("#result-4").innerHTML =
    `Chuỗi đảo ngược có kết quả là: ` + ketQua;
}

// Cau 6:

function soNguyenDuongLonNhat() {
  var ketQua = 0;
  var dem = 0;
  for (var i = 1; i <= 100; i++) {
    ketQua += i;
    if (ketQua >= 100) {
      break;
    }
    dem++;
  }
  document.querySelector(
    "#result-5"
  ).innerHTML = `Số nguyên dương lớn nhất là: ${dem} `;
}

// Cau 7:
function inBangCuuChuong() {
  var number = document.querySelector("#nhap-so-n").value * 1;
  var ketQua = "";
  for (var i = 0; i <= 10; i++) {
    if (number > 0) {
      ketQua += `${number} * ${i} = ` + number * i;
    }
    ketQua += `<br/>`;
  }
  document.querySelector(
    "#result-6"
  ).innerHTML = `Bảng cửu chương là: <br/> ${ketQua}`;
}

// Cau 8:
// Cách 1:
var cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
document.getElementById("chuoicards").innerHTML = cards;

function xaoLaBai() {
  ranDom(cards);
  document.getElementById("chuoicards").innerHTML = cards;
}
function phatLaBai() {
  var ketQua = "";
  var player1 = [];
  var player2 = [];
  var player3 = [];
  var player4 = [];
  for (var i = 0; i < cards.length; i += 4) {
    player1.push(cards[i]);
  }
  for (var i = 1; i < cards.length; i += 4) {
    player2.push(cards[i]);
  }
  for (var i = 2; i < cards.length; i += 4) {
    player3.push(cards[i]);
  }
  for (var i = 3; i < cards.length; i += 4) {
    player4.push(cards[i]);
  }
  ketQua = `player1 = [${player1}] <br/>player2 = [${player2}] <br/> player3 = [${player3}] <br/> player4 = [${player4}]`;
  document.querySelector("#result-7").innerHTML = ketQua;
}
// Cách 2:
// var cards = [
//   "4K",
//   "KH",
//   "5C",
//   "KA",
//   "QH",
//   "KD",
//   "2H",
//   "10S",
//   "AS",
//   "7H",
//   "9K",
//   "10D",
// ];
// document.getElementById("chuoicards").innerHTML = cards;

// function xaoLaBai() {
//   ranDom(cards);
//   document.getElementById("chuoicards").innerHTML = cards;
// }
// function chiaCacLaBai(list) {
//   var ketQua = "";
//   var player1 = [];
//   var player2 = [];
//   var player3 = [];
//   var player4 = [];
//   for (i = 0; i < list.length; i++) {
//     player1 = list[0] + `, ` + list[4] + `, ` + list[8];
//     player2 = list[1] + `, ` + list[5] + `, ` + list[9];
//     player3 = list[2] + `, ` + list[6] + `, ` + list[10];
//     player4 = list[3] + `, ` + list[7] + `, ` + list[11];
//     break;
//   }
//   ketQua = `player1 = [${player1}] <br/>player2 = [${player2}] <br/> player3 = [${player3}] <br/> player4 = [${player4}]`;
//   return ketQua;
// }

// function phatLaBai() {
//   var ketQua = chiaCacLaBai(cards);
//   document.querySelector("#result-7").innerHTML = ketQua;
// }

// Cau 9:
function timSoGaCho() {
  var m = document.getElementById("sogacho").value * 1;
  var n = document.getElementById("tongsochan").value * 1;
  var soGa = 0;
  var soCho = 0;
  for (var i = 0; i <= m; i++) {
    if (i * 2 + (m - i) * 4 == n) {
      soGa = i;
      soCho = m - i;
    }
  }
  document.getElementById(
    "result-8"
  ).innerHTML = `Số gà là: ${soGa}, số chó là: ${soCho}`;
}

// Cau 10:
// Kim phút:
// 60 phút = 360 độ;
// 1 phút = 6 độ;
// Kim giờ :
// 12 giờ = 360 độ;
// 1 giờ = 30 độ;
// 1 phút =  0.5 độ;
function timGocLech() {
  var soGio = document.getElementById("txt-so-gio").value * 1;
  var soPhut = document.getElementById("txt-so-phut").value * 1;
  var soDoKimPhut = 6 * soPhut;
  var soDoKimGio = 0.5 * (soGio * 60 + soPhut);
  var ketQua = "";
  if (soGio >= 0 && soPhut >= 0) {
    ketQua = `Góc tạo giữa kim giờ và kim phút là: ${Math.abs(
      soDoKimPhut - soDoKimGio
    )} độ`;
  } else {
    ketQua = `Giờ và phút không hợp lý`;
  }
  document.getElementById("result-9").innerHTML = ketQua;
}
